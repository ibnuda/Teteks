# API PLAN
Somewhere around 3 main parts:
- Authentication and Authorization.
- Disk.
- Item. 

## Auth.
This piece of shit maybe should use JWT.

#### Pros
- Well, cooler, buddy.
  Much cooler than the normal application.
- Standard?
- Federation?

#### Cons
- Completely new to this tech.
- Have to have auth server.
- Complexity increases.


## Disk
The root of the items. Just like `$HOME` on *nix env.

### Common Command
- Information of the disk.
- List child.

#### Information of the disk.
Looks like this from OneDrive API. Just simplify it as you wish.
```json
{
  "id": "string",
  "driveType": "personal | business",
  "owner": { "@odata.type": "oneDrive.identitySet" },
  "quota": { "@odata.type": "oneDrive.quota" },

  "items": [ { "@odata.type": "oneDrive.item" } ],
  "root": { "@odata.type": "oneDrive.item" },
  "special": [ { "@odata.type": "oneDrive.item" }]
}
```#### List ChildLooks like this.```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "value": [
    {"name": "myfile.jpg", "size": 2048, "file": {} },
    {"name": "Documents", "folder": { "childCount": 4} },
    {"name": "Photos", "folder": { "childCount": 203} },
    {"name": "my sheet(1).xlsx", "size": 197 }
  ],
  "@odata.nextLink": "https://..."
}
```

## Item
Represents an item in the disk. Just like `/etc/hostname` or whatever.


