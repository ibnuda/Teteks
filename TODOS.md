# General
- **REFACTOR THIS BULLSHIT.**
- Use GraphDB for directory thing.
- Auth.

## Domain
- ~~Add directory datatypes.~~
- ~~Add file datatypes.~~
- ~~Maybe result types.~~
- Use FileInfo and DirectoryInfo.

## Command (or something like that)
- ~~Fix `runWithEx`~~
- ~~List files.~~
- ~~List directories.~~
- ~~List files AND directory.~~
- ~~Get the details of files and directories.~~
- ~~Get the right URI.~~
- ~~Refresh the file list after upload.~~
- ~~For CommandDir, use `string` (or `string list`) instead of `Stream`.~~
- ~~Add delete and rename folder.~~
- ~~Add rename file.~~
- Perhaps I have to save it to the database.
- In addition to the point above, use GUID for directory indentification.
- ~~Each and every command should return the result of computation. i.e. when create dir, the return value should be wrapped in a `CommandResult` instance like the following :~~
  ```
  let computationResult =
    { Command = "rename"
      Target = [ "path"; "of"; "target" ]
      Result = "success" }
  ```

## Results Representation
- ~~Perhaps json for the relevant information.~~
- ~~Basic HTML for everything.~~

## Request Handlers
- ~~"How do you handle a request that requests the content of a directory?"~~
- Special character in URL to normal or something. (%20 to " ")

## Json Interface
- ~~Story create, read, update.~~
- Story delete?
- ~~Disk content list.~~
- ~~Disk create and rename directory.~~

## Misc
- ~~Single attributed HTML.~~
- Use OneDrive API Docs.

