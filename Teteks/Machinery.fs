﻿module Machinery

open Freya.Core
open Freya.Core.Operators
open Freya.Machines.Http
open Freya.Types.Http
open Freya.Routers.Uri.Template
open Freya.Optics.Http
open Representasi
open HandlerStory
open HandlerDir

let areYouLost = 
  freya {
    let! url = Request.path_ |> Freya.Optic.get
    return url = "/404"
  }

let apiEnd = 
  freyaMachine { 
    methods [ POST ]
    created true
    doPost (ignore <!> apiStoryCreateHandler)
    handleCreated (jsonRepr <!> apiStoryCreateHandler)
  }

let apiEndStory = 
  freyaMachine { 
    methods [ GET; POST ]
    created true
    handleOk (jsonRepr <!> apiStoryReadHandler)
    doPost (ignore <!> apiStoryUpdateHandler)
    handleCreated (jsonRepr <!> apiStoryUpdateHandler)
  }

let normHome = 
  freyaMachine { 
    methods [ GET; POST ]
    handleOk (homeRepr <!> Freya.init (None))
    created true
    doPost (ignore <!> storyCreateHandler)
    handleCreated (storyRepr <!> storyCreateHandler)
  }

let normStoryRead = 
  freyaMachine { 
    methods [ GET ]
    created true
    handleOk (storyRepr <!> apiStoryReadHandler)
  }

let normStoryEdit = 
  freyaMachine { 
    methods [ GET; POST ]
    handleOk (editRepr <!> apiStoryReadHandler)
    created true
    doPost (ignore <!> storyUpdateHandler)
    handleCreated (storyRepr <!> storyUpdateHandler)
  }

let nothing = 
  freyaMachine { 
    methods [ GET ]
    exists areYouLost
    handleNotFound (storyRepr <!> lostHandler)
  }

let downFile = 
  freyaMachine { 
    methods [ GET ]
    handleOk (downRepr <!> dirDownloadFileHandler)
  }

let listContent = 
  freyaMachine {
    methods [ GET; POST ]
    created true
    doPost (ignore <!> dirUploadFileHandler)
    handleCreated (listContentRepr <!> dirDirInformationHandler)
    handleOk (listContentRepr <!> dirDirInformationHandler)
  }

let listContentAfterCreated =
  freyaMachine {
    methods [ POST ]
    created true
    doPost (ignore <!> dirCreateDirHandler)
    handleCreated (listContentRepr <!> dirDirInformationHandler)
  }

let listContentAfterRenamed =
  freyaMachine {
    methods [ POST ]
    created true
    doPost (ignore <!> dirRenameDirHandler)
    handleCreated (listContentRepr <!> dirDirInformationHandler)
  }

let apiDirDirInformationMachine =
  freyaMachine {
    methods [ GET ]
    handleOk (jsonRepr <!> dirDirInformationHandler)
  }

let apiDirRenameItemMachine = 
  freyaMachine {
    methods [ POST ]
    doPost (ignore <!> apiDirRenameItemHandler)
    handleCreated (jsonRepr <!> apiDirRenameItemHandler)
    created true
  }

let apiDirCreateDirMachine = 
  freyaMachine {
    methods [ POST ]
    doPost (ignore <!> apiDirCreateDirHandler)
    handleCreated (jsonRepr <!> apiDirCreateDirHandler)
    created true
  }

let apiDirRemoveItemMachine = 
  freyaMachine {
    methods [ POST ]
    doPost (ignore <!> apiDirRemoveItemHandler)
    handleCreated (jsonRepr <!> apiDirRemoveItemHandler)
    created true
  }

let ceritaRuter = 
  freyaRouter { 
    resource "/story" normHome
    resource "/story/" normHome
    resource "/story{/url}" normStoryRead
    resource "/story{/url}/edit" normStoryEdit
    resource "/disk/list{/listUrl*}" listContent
    resource "/disk/down{/listUrl*}" downFile
    resource "/disk/create{/listUrl*}" listContentAfterCreated
    resource "/disk/rename{/listUrl*}" listContentAfterRenamed
    resource "/api/story" apiEnd
    resource "/api/story/" apiEnd
    resource "/api/story{/url}" apiEndStory
    resource "/api/disk/list{/listUrl*}" apiDirDirInformationMachine
    resource "/api/disk/create/" apiDirCreateDirMachine
    resource "/api/disk/rename/" apiDirRenameItemMachine
    resource "/api/disk/remove/" apiDirRemoveItemMachine
    resource "{/listUrl*}" nothing
  }

let api = ceritaRuter
