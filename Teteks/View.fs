﻿module View

open Utilities.DirUtil
open Utilities.TextUtil
open FsHtml
open DomainStory
open DomainDir

let css = """
  html { -webkit-text-size-adjust: 100%; padding-bottom: 4em; padding-top: 3em }
  body { font-family: sans-serif; line-height: 1.5em; max-width: 40em; padding: 0 2%; margin: auto }
  fieldset { margin-bottom: 1.5em }
  textarea { height: 10em }
  dt { font-weight: bold }
  .text-input { width: 96%; display: block; padding: .5em 1%; margin-bottom: 1.5em; font-size: 1em }
  .important { color: red }
  .footer { text-align: right }
  .centered { text-align: center }"""

let siteStyle = 
  style [ "type" %= "text/css"
          text css ]

let met valueName valueContent = 
  meta [ "name" %= (valueName |> safeText)
         "content" %= (valueContent |> safeText) ]

let metameta = 
  [ met "viewport" "width=device-width, initial-scale=1.0"
    met "description" "content desc."
    met "author" "ibnu." ]

let metatw (story : Story) = 
  [ met "twitter:card" story.TCard
    met "twitter:title" story.TTitle
    met "twitter:author" story.TAuthor
    met "twitter:description" story.TDesc ]

let messages (message : Message) = 
  html [ head (metameta @ [ title [ text message.Action ]
                            siteStyle ])
         body [ text (message.Content + " on ")
                a <| href (urlStory + message.Url) (text message.Url)
                br []
                text "Back to "
                a <| href urlStory (text urlStory) ] ]

let textAreaSimple cls name tex = 
  textarea [ "class" %= cls
             "name" %= name
             text tex ]

let labelSimple name tex = 
  label [ "for" %= name
          text tex ]

let inputSimple cls name typ value = 
  input [ "class" %= cls
          "name" %= name
          "type" %= typ
          "value" %= value ]

let labelInputSimple name tex value = 
  [ labelSimple name tex
    inputSimple "text-input" name "text" value ]

let lostMessage = 
  messages (Message.Create("Nah", "Nah", "Won't happen."))
  
let newForm = 
  html [ head (metameta @ [ title [ text "Something" ]
                            siteStyle ])
         body [ h1 [ text "Teteks" ]
                p [ text "Clone of "
                    a <| href "http://txti.es" (text "txti.es") ]
                h2 [ text "Write here." ]
                form [ "method" %= "POST"
                       "enctype" %= "multipart/form-data"
                       "action" %= urlStory
                       textAreaSimple "text-input" "content" ""
                       fieldset [ legend [ text "Pass and URL" ]
                                  labelSimple "url" "URL. Optional."
                                  inputSimple "text-input" "url" "text" ""
                                  labelSimple "password" "Password. Optional."
                                  inputSimple "text-input" "password" "text" "" ]
                       fieldset [ legend [ text "Twitter"]
                                  labelSimple "tcard" "Twitter card. Optional."
                                  inputSimple "text-input" "tcard" "text" ""
                                  labelSimple "tauthor" "Author. Optional."
                                  inputSimple "text-input" "tauthor" "text" ""
                                  labelSimple "ttitle" "Title. Optional."
                                  inputSimple "text-input" "ttitle" "text" ""
                                  labelSimple "tdesc" "Desc. Optional."
                                  inputSimple "text-input" "tdesc" "text" "" ]
                       input [ "type" %= "submit"
                               "value" %= "Save" ] ] ] ]

let editForm (story : Story) = 
  html [ head (metameta @ (metatw story) @ [ title [ text story.Url ]
                                             siteStyle ])
         body [ h1 [ text "Teteks" ]
                p [ text "Clone of "
                    a <| href "http://txti.es" (text "txti.es") ]
                h2 [ text "Write here." ]
                form [ "method" %= "POST"
                       "enctype" %= "multipart/form-data"
                       "action" %= (urlEdit story.Url)
                       textAreaSimple "text-input" "content" story.Content
                       fieldset [ legend [ text "Password and URL" ]
                                  labelSimple "password" "Password to change. Must be filled."
                                  inputSimple "text-input" "password" "text" ""
                                  labelSimple "url" "URL."
                                  inputSimple "text-input" "url" "text" story.Url
                                  labelSimple "newpassword" "New password"
                                  inputSimple "text-input" "newpassword" "text" "" ]
                       fieldset [ legend [ text "Twitter"]
                                  labelSimple "tcard" "Twitter card. Optional."
                                  inputSimple "text-input" "tcard" "text" story.TCard
                                  labelSimple "tauthor" "Author. Optional."
                                  inputSimple "text-input" "tauthor" "text" story.TAuthor
                                  labelSimple "ttitle" "Title. Optional."
                                  inputSimple "text-input" "ttitle" "text" story.TTitle
                                  labelSimple "tdesc" "Desc. Optional."
                                  inputSimple "text-input" "tdesc" "text" story.TDesc ]
                       inputSimple "hidden" "oldurl" "hidden" story.Url
                       input [ "type" %= "submit"
                               "value" %= "Save" ] ] ] ]

let storyView (story : DomainStory) = 
  match story with
  | Story story -> 
    html [ head (metameta @ (metatw story) @ [ title [ text story.TTitle ]
                                               siteStyle ])
           body [ (toHtml >> text) story.Content ] ]
  | Message fail -> messages fail
  | _ -> lostMessage

let editView (dataDomain : DomainStory) = 
  match dataDomain with
  | Story storyToEdit -> editForm storyToEdit
  | Message fail -> messages fail
  | _ -> lostMessage

let contentRowTable (listInfo : ContentInformation)  =
  match listInfo with 
  | Dir dirInfo ->
    tr [
      td [ a <| href (urlDiskList (urlize dirInfo.FullName)) (text dirInfo.Name) ]
      td [ text "-" ]
      td [ text (dirInfo.LastWriteTime.ToString()) ]
    ]
  | Fil filInfo ->
    tr [
      td [ a <| href (urlDiskDown (urlize filInfo.FullName)) (text filInfo.Name) ]
      td [ text ((filInfo.Length / 1024L).ToString()) ]
      td [ text (filInfo.CreationTime.ToString()) ]
    ]

let filDetail (data : DomainDir) =
  match data with
  | ContentInfo (_, contentInfo) ->
    List.map contentRowTable contentInfo
  | _ ->
    [ tr [
        td [ text "Nothing" ]
        td [ text "To" ]
        td [ text "See" ] ] ]

let listContentView (contentList : DomainDir)=
  let path = CommandDir.dirName contentList
  html [ head (metameta @ [ title [ text "Dir Content"]
                            siteStyle ])
         body [ h1 [ text "Dirs and Files" ]
                // make it a breadcrumb or something.
                // preferably with link.
                h2 [ text ("/" + path) ]
                form [ "enctype" %= "multipart/form-data"
                       "method" %= "POST"
                       "action" %= (urlDiskList path)
                       fieldset [ legend [ text "Upload" ]
                                  labelSimple "fileupload" "File Upload"
                                  inputSimple "" "fileupload" "file" "" ]
                       input [ "type" %= "submit"
                               "value" %= "Upload" ] ]
//                form [ "enctype" %= "multipart/form-data"
//                       "method" %= "POST"
//                       "action" %= (urlDiskCreate path)
//                       fieldset [ legend [ text "New Folder" ]
//                                  labelSimple "dirname" "New Folder Name"
//                                  inputSimple "" "dirname" "" "" ]
//                       input [ "type" %= "submit" 
//                               "value" %= "Create" ] ]
//                form [ "enctype" %= "multipart/form-data"
//                       "method" %= "POST"
//                       "action" %= (urlDiskRename path)
//                       fieldset [ legend [ text "Rename Folder" ]
//                                  labelSimple "renamed" "Folder Name"
//                                  inputSimple "" "renamed" "" "" ]
//                       input [ "type" %= "submit" 
//                               "value" %= "Rename" ] ]
                table [
                  thead [
                    tr [
                      th [ text "Filename" ]
                      th [ text "Filesize" ]
                      th [ text "Last edit" ] ] ]
                  tbody (filDetail contentList) ] ] ]

let downView message =
  messages (Message.Create("Unggah", "/berkas/", message))

let story = storyView >> Html.ToString
let edit = editView >> Html.ToString
let down = downView >> Html.ToString
let listContent = listContentView >> Html.ToString
let newStory = Html.ToString newForm
