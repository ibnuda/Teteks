﻿module Representasi

open Freya.Machines.Http
open Chiron

open Utilities.RepUtil
open DomainDir

let storyRepr story = createRepHtml (View.story story)

let editRepr story = createRepHtml (View.edit story)

let homeRepr (_ : 'a option) = createRepHtml View.newStory

let inline jsonRepr thing =
  createRepJson ((Json.serialize >> Json.format) thing)

let downRepr (data : DomainDir) = 
  match data with
  | DomainDir.FileContent (binary : byte [], _ : string) ->
    createRepBin binary
  | DomainDir.FileName thing | DomainDir.ErrorMessage thing ->
    createRepHtml (View.down thing)
  | DomainDir.ContentInfo _ | DomainDir.Remove _
  | DomainDir.Create _ | DomainDir.Rename _ 
  | DomainDir.CommieRest _ ->
    Representation.empty

let listContentRepr (dirContent : DomainDir) = createRepHtml (View.listContent dirContent)

