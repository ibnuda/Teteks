﻿module CommandStory

open System.IO
open HttpMultipartParser

open Utilities.TextUtil
open DomainStory

let getStoryFrom (url : string) = 
  let storyFromDb = Database.getStory url Database.gcont
  match storyFromDb with
  | Some story -> 
    Some(Story.Create(story.Url, story.Isi, story.TwCard, story.TwTitle, story.TwDesc, story.TwAuthor))
  | None -> None

let getStory url = 
  let story = getStoryFrom url
  match story with
  | Some cerita -> DomainStory.Story cerita
  | None -> 
    let message = Message.Create("Get story", url, "There's no such thing ")
    DomainStory.Message message

let insertStoryToDb (newStory : NewStory option) = 
  match newStory with
  | Some newStory ->
    let url = randomFromOption newStory.Url 16
    let password = randomFromOption newStory.Password 8
    let content = defaultArg newStory.Content "Shouldn't be possible, tbphwymphamalam."
    let ta = defaultArg newStory.TAuthor "@ibnuda"
    let tc = defaultArg newStory.TCard "Teteks."
    let td = defaultArg newStory.TDesc "Teteks."
    let tt = defaultArg newStory.TTitle "Teteks."

    Database.saveStory(url, password, content, tc, tt, ta, td) Database.gcont
  | None -> None

let insertStory (newStory : NewStory option) = 
  match insertStoryToDb newStory with
  | None -> 
    let failToSave = Message.Create("Save story", "", "Fail to save ")
    DomainStory.Message failToSave
  | Some thing -> 
    let notYetSaved = Message.Create("Save story", thing.Url, sprintf "Please open the story ")
    DomainStory.Message notYetSaved

let editStoryInDb oldUrl (editStory : EditStory option) = 
  match editStory with
  | Some editStory ->
    let url = defaultArg editStory.Url oldUrl
    let newUrl = sanitizeUrl url
    let password = editStory.Password
    let newPassword = defaultArg editStory.NewPassword password
    let content = defaultArg editStory.Content "Shouldn't be possible, tbphwymphamalam."
    let ta = defaultArg editStory.TAuthor "@ibnuda"
    let tc = defaultArg editStory.TCard "Teteks"
    let td = defaultArg editStory.TDesc "Teteks"
    let tt = defaultArg editStory.TTitle "Teteks"
    
    url, Some (Database.editStory(oldUrl, newUrl, password, newPassword, content, tt, tc, ta, td) Database.gcont)
  | None -> oldUrl, None

let editStory oldUrl (editStory : EditStory option) = 
  match editStoryInDb oldUrl editStory with
  | _, Some story -> 
    let messageEdited = Message.Create("Edit story", (story.Value).Url, "Edited ")
    DomainStory.Message messageEdited
  | oldUrl, None -> 
    let failToEdit = Message.Create("Edit story", oldUrl, "Couldn't save the story ")
    DomainStory.Message failToEdit

let notFound url = 
  DomainStory.Message (Message.Create("Lost", url, "Are you lost "))

let readStream (stream : Stream) : Async<string> = 
  use reader = new StreamReader(stream)
  reader.ReadToEndAsync() |> Async.AwaitTask

let runFromEx (f : unit -> 'T Option) =
  try
    f ()
  with
  | :? MultipartParseException -> None
  | :? System.NullReferenceException -> None

let newStoryEx input () =
  let parser = new MultipartFormDataParser(input)
  let content = parser.GetParameterValue("content") |> emptyAsNone
  let url = parser.GetParameterValue("url") |> emptyAsNone
  let password = parser.GetParameterValue("password") |> emptyAsNone
  let ta = parser.GetParameterValue("tauthor") |> emptyAsNone
  let tc = parser.GetParameterValue("tcard") |> emptyAsNone
  let td = parser.GetParameterValue("tdesc") |> emptyAsNone
  let tt = parser.GetParameterValue("ttitle") |> emptyAsNone
  NewStory.Create(url, password, content, tc, tt, td, ta) |> Some

let editStoryEx input () =
  let parser = new MultipartFormDataParser(input)
  let content = parser.GetParameterValue("content") |> emptyAsNone
  let url = parser.GetParameterValue("url") |> emptyAsNone
  let password = parser.GetParameterValue("password")
  let newpassword = parser.GetParameterValue("newpassword") |> emptyAsNone
  let ta = parser.GetParameterValue("tauthor") |> emptyAsNone
  let tc = parser.GetParameterValue("tcard") |> emptyAsNone
  let td = parser.GetParameterValue("tdesc") |> emptyAsNone
  let tt = parser.GetParameterValue("ttitle") |> emptyAsNone
  EditStory.Create(url, password, newpassword, content, tt, tc, ta, td) |> Some

let newStoryFromForm (input : Stream) = runFromEx (newStoryEx input)

let editStoryFromForm (input : Stream) = runFromEx (editStoryEx input)
