﻿module Utilities

module TextUtil =
  open System
  open System.Text.RegularExpressions

  open FSharp.Markdown

  let reg = new Regex("[^a-zA-Z0-9-\.]")
  let sanitizeUrl url = reg.Replace(url, "-")

  let randomString = 
    let chars : string = "pyfgcrlaoeuidhtns-qjkxbmwvzPYFGCRLAOEUIDHTNSQJKXBMWVZ7531902468"
    let charsLen : int = chars.Length
    let rand : Random = System.Random()
    fun len -> 
      let randomChars : char [] = 
        [| for _ in 0..len -> chars.[rand.Next(charsLen)] |]
      new System.String(randomChars)

  let randomFromOption thing len = 
    match thing with
    | Some thing -> 
      if thing.Equals "" then (randomString len)
      else sanitizeUrl thing
    | None -> randomString len

  let decode = System.Net.WebUtility.UrlDecode

  let emptyAsNone str =
    if String.IsNullOrEmpty str then None
    else Some str

  let lostToString = List.fold (fun acc x -> acc + "/" + x) ""

  let toHtml md = 
    md
    |> System.Net.WebUtility.HtmlEncode
    |> (Markdown.Parse >> Markdown.WriteHtml)
    |> System.Net.WebUtility.HtmlDecode

module RequestUtil =
  open System.IO
  open Freya.Core
  open Freya.Core.Operators
  open Freya.Optics.Http
  open Freya.Routers.Uri.Template
  open Chiron

  let reqUrl = Freya.Optic.get (Route.atom_ "url")
  let urlList = Freya.Optic.get (Route.list_ "listUrl")

  let defArg url = defaultArg url "" |> System.Net.WebUtility.UrlDecode
  let defArgList urlList = defaultArg urlList [""] |> List.map System.Net.WebUtility.UrlDecode

  let requestUrl = Freya.map(reqUrl, defArg)
  let requestUrlList = Freya.map(urlList, defArgList)

  let inline payload () = 
    function 
    | stream -> Json.tryParse ((new StreamReader(stream : Stream)).ReadToEnd())
    >> function 
    | Choice1Of2 json -> Json.tryDeserialize json
    | Choice2Of2 stri -> Choice2Of2 stri
    >> function 
    | Choice1Of2 thing -> Some thing
    | _ -> None
    <!> Freya.Optic.get Request.body_

module DirUtil = 
  open System.IO

  let dir = "C:\\tools\\"
  let urlize (str : string) = str.Replace(dir, "").Replace("\\", "/")
  let normalize (str : string) = str.Replace("/", "\\")
  let urlSite = "http://localhost:6777/"
  let urlStory = urlSite + "story/"
  let urlEdit url = urlStory + url + "/edit"
  let urlDiskList url = urlSite + "disk/list/" + url
  let urlDiskDown url = urlSite + "disk/down/" + url
  let urlDiskCreate url = urlSite + "disk/create/" + url
  let urlDiskRename url = urlSite + "disk/rename/" + url

  let urlListToDiskPath list =
    dir :: list
    |> Array.ofList
    |> Path.Combine

  let urlListToDiskPathAndAppend list target =
    List.append (dir :: list) target
    |> Array.ofList 
    |> Path.Combine

  let parentDirectory (l : string list) =
    match l.Length > 0 with
    | true -> l |> List.rev |> List.tail |> List.rev
    | false -> l

module RepUtil =
  open System.Text
  open Freya.Machines.Http
  open Freya.Types.Http

  let toByte (data : string) = Encoding.UTF8.GetBytes(data)

  let createRepHtml data = 
    { Data = toByte data
      Description =
        { Charset = Some Charset.Utf8
          Encodings = None
          MediaType = Some MediaType.Html
          Languages = None } }

  let createRepJson data = 
    { Data = toByte data
      Description =
        { Charset = Some Charset.Utf8
          Encodings = None
          MediaType = Some MediaType.Json
          Languages = None } }

  let createRepBin (data : byte []) = 
    { Description = 
        { Charset = None
          Encodings = None
          MediaType = None
          Languages = None }
      Data = data }
