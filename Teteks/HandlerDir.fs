﻿module HandlerDir

open Freya.Core
open Freya.Core.Operators
open Freya.Optics.Http
open Freya.Core

open Utilities.RequestUtil

let dirDownloadFileHandler =
  Freya.memo (requestUrlList >>= fun u ->
              Freya.fromAsync (u, Pemroses.dirDownloadFile))

let dirDirInformationHandler =
  Freya.memo (requestUrlList >>= fun uL ->
              Freya.fromAsync (uL, Pemroses.dirDirInformation))

let dirUploadFileHandler =
  Freya.memo (Freya.Optic.get Request.body_ >>= fun rb ->
              requestUrlList >>= fun uL ->
              Freya.fromAsync ((uL, rb), Pemroses.dirUploadFile))

let dirCreateDirHandler =
  Freya.memo (requestUrlList >>= fun uL ->
              Freya.Optic.get Request.body_ >>= fun rb ->
              Freya.fromAsync ((uL, rb), Pemroses.dirCreateDir))

let dirRenameDirHandler =
  Freya.memo (requestUrlList >>= fun uL ->
              Freya.Optic.get Request.body_ >>= fun rb ->
              Freya.fromAsync ((uL, rb), Pemroses.dirRenameItem))

let dirRemoveItemHandler =
  Freya.memo (requestUrlList >>= fun uL ->
              Freya.Optic.get Request.body_ >>= fun rb ->
              Freya.fromAsync ((uL, rb), Pemroses.dirDeleteItem))

let apiDirCreateDirHandler =
  Freya.memo (payload () >>= fun payloadJson ->
              Freya.fromAsync (payloadJson, Pemroses.apiDirCreate))

let apiDirRenameItemHandler =
  Freya.memo (payload () >>= fun payloadJson ->
              Freya.fromAsync (payloadJson, Pemroses.apiDirRenameItem))

let apiDirRemoveItemHandler =
  Freya.memo (payload () >>= fun payloadJson ->
              Freya.fromAsync (payloadJson, Pemroses.apiDirRemoveItem))
