﻿module Pemroses

open System.IO

open DomainStory
open DomainDir
open CommandStory
open CommandDir

type StoryProtocol = 
  | Create of AsyncReplyChannel<DomainStory> * NewStory option
  | Update of AsyncReplyChannel<DomainStory> * string * EditStory option
  | Read of AsyncReplyChannel<DomainStory> * string
  | Lost of AsyncReplyChannel<DomainStory> * string

let storyProcessor (mailbox : MailboxProcessor<StoryProtocol>) = 
  let reply (channel : AsyncReplyChannel<_>) thing = 
    channel.Reply thing
    thing
  
  let create channel newStory = insertStory newStory |> reply channel
  let update channel oldUrl editToStory = editStory oldUrl editToStory |> reply channel
  let read channel url = getStory url |> reply channel
  let lost channel url = notFound url |> reply channel
  
  let rec loop _ = 
    async.Bind(mailbox.Receive(), 
               function 
               | Lost(channel, url) -> loop (lost channel url)
               | Create(channel, newStory) -> loop (create channel newStory)
               | Update(channel, oldUrl, editToStory) -> loop (update channel oldUrl editToStory)
               | Read(channel, url) -> loop (read channel url))

  loop (DomainStory.Message (Message.Create("", "/", "Nothing")))

type DiskProtocol = 
  | Upload of AsyncReplyChannel<DomainDir> * string list * Stream
  | Download of AsyncReplyChannel<DomainDir> * string list
  | Information of AsyncReplyChannel<DomainDir> * string list
  | CreateDir of AsyncReplyChannel<DomainDir> * string list * Stream
  | RenameItem of AsyncReplyChannel<DomainDir> * string list * Stream
  | DeleteItem of AsyncReplyChannel<DomainDir> * string list * Stream
  | ApiCreateDir of AsyncReplyChannel<DomainDir> * JsonCreateDir option
  | ApiRenameItem of AsyncReplyChannel<DomainDir> * JsonRenameItem option
  | ApiDeleteItem of AsyncReplyChannel<DomainDir> * JsonRemoveItem option

let diskProcessor (mailbox : MailboxProcessor<DiskProtocol>) = 
  let reply (channel : AsyncReplyChannel<_>) data =
    channel.Reply data
    data

  let uploadFile channel path stream = writeFile path stream |> reply channel
  let downloadFile channel path = readFile path |> reply channel
  let informationDir channel path = contentListOfDirectory path |> reply channel
  let createDir channel path stream = createDirectoryFromForm path stream |> reply channel
  let renameItem channel path stream = renameItemFromForm path stream |> reply channel
  let deleteItem channel path stream = deleteItemFromForm path stream |> reply channel

  let apiCreateItem channel j = apiCreateDirectory j |> reply channel
  let apiRenameItem channel j = apiRenameItem j |> reply channel
  let apiDeleteItem channel j = apiDeleteItem j |> reply channel

  let rec loop _ =
    async.Bind(mailbox.Receive(),
               function
               | Upload(channel, path, stream) -> loop (uploadFile channel path stream)
               | Download(channel, path) -> loop (downloadFile channel path)
               | Information(channel, path) -> loop (informationDir channel path)
               | CreateDir(channel, path, stream) -> loop (createDir channel path stream)
               | RenameItem(channel, path, stream) -> loop (renameItem channel path stream)
               | DeleteItem(channel, path, stream) -> loop (deleteItem channel path stream)
               | ApiCreateDir(channel, j) -> loop (apiCreateItem channel j)
               | ApiRenameItem(channel, j) -> loop (apiRenameItem channel j)
               | ApiDeleteItem(channel, j) -> loop (apiDeleteItem channel j) )

  loop (DomainDir.FileName "")

let diskState = MailboxProcessor.Start(diskProcessor)
let storyState = MailboxProcessor.Start(storyProcessor)

let storyCreate newstory =
  storyState.PostAndAsyncReply(fun channel -> Create(channel, newstory))
let storyUpdate (oldurl, edittostory) =
  storyState.PostAndAsyncReply(fun channel -> Update(channel, oldurl, edittostory))
let storyRead url =
  storyState.PostAndAsyncReply(fun channel -> Read(channel, url))
let lostLostLost url =
  storyState.PostAndAsyncReply(fun channel -> Lost(channel, url))

let dirUploadFile (path, stream) =
  diskState.PostAndAsyncReply(fun channel -> Upload(channel, path, stream))
let dirDownloadFile path =
  diskState.PostAndAsyncReply(fun channel -> Download(channel, path))
let dirDirInformation path =
  diskState.PostAndAsyncReply(fun channel -> Information(channel, path))
let dirCreateDir (path, stream) =
  diskState.PostAndAsyncReply(fun channel -> CreateDir(channel, path, stream))
let dirRenameItem (path, stream) =
  diskState.PostAndAsyncReply(fun channel -> RenameItem(channel, path, stream))
let dirDeleteItem (path, stream) =
  diskState.PostAndAsyncReply(fun channel -> DeleteItem(channel, path, stream))

let apiDirCreate json =
  diskState.PostAndAsyncReply(fun channel -> ApiCreateDir(channel, json))
let apiDirRemoveItem json =
  diskState.PostAndAsyncReply(fun channel -> ApiDeleteItem(channel, json))
let apiDirRenameItem json =
  diskState.PostAndAsyncReply(fun channel -> ApiRenameItem(channel, json))
