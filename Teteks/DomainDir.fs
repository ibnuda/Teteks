﻿module DomainDir

open System.IO
open Chiron

open Utilities.DirUtil

type ContentInformation =
  | Fil of FileInfo
  | Dir of DirectoryInfo
  static member ToJson info = 
    match info with
    | Fil file -> 
      json {
        do! Json.write "type" "file"
        do! Json.write "name" file.Name
        do! Json.write "size" ((file.Length / 1024L).ToString() + "kb")
        do! Json.write "lastmod" (file.LastWriteTime.ToString())
      }
    | Dir dir ->
      json {
        do! Json.write "type" "directory"
        do! Json.write "nome" dir.Name
        do! Json.write "size" "-"
        do! Json.write "lastmod" (dir.LastWriteTime.ToString())
      }

type JsonCreateDir =
  { PathParent : string list
    PathDir : string list }
  static member ToJson (j : JsonCreateDir) =
    json {
      do! Json.write "pathparent" j.PathParent
      do! Json.write "pathdir" j.PathDir
    }
  static member FromJson (_ : JsonCreateDir) =
    json {
      let! parentpath = Json.read "pathparent"
      let! dirname = Json.read "pathdir"
      return { PathParent = parentpath; PathDir = dirname }
    }

type JsonRemoveItem =
  { PathParent : string list
    PathItem : string }
  static member ToJson (j : JsonRemoveItem) =
    json {
      do! Json.write "pathparent" j.PathParent
      do! Json.write "pathitem" j.PathItem
    }
  static member FromJson (_ : JsonRemoveItem) =
    json {
      let! parentpath = Json.read "pathparent"
      let! itemname = Json.read "pathitem"
      return { PathParent = parentpath; PathItem = itemname }
    }

type JsonRenameItem =
  { PathParentOld : string list
    PathParentNew : string list
    PathItemOld : string
    PathItemNew : string }
  static member ToJson (j : JsonRenameItem) =
    json {
      do! Json.write "pathparentold" j.PathParentOld
      do! Json.write "pathparentnew" j.PathParentNew
      do! Json.write "pathitemold" j.PathItemOld
      do! Json.write "pathitemnew" j.PathItemNew
    }
  static member FromJson (_ : JsonRenameItem) =
    json {
      let! parentpathold = Json.read "pathparentold"
      let! parentpathnew = Json.read "pathparentnew"
      let! oldname = Json.read "pathitemold"
      let! newname = Json.read "pathitemnew"
      return { PathParentOld = parentpathold; PathParentNew = parentpathnew; PathItemOld = oldname; PathItemNew = newname }
    }

type CommieRest =
  { Command : string // Should be a discriminated union.
    Target : string list // Could be a path or name of directory/file
    Result : string } // Either Success of Failure
  static member Create (command, target, result) =
    { Command = command; Target = target; Result = result }
  static member ToJson (cr : CommieRest) =
    json {
      do! Json.write "command" cr.Command
      do! Json.write "target" cr.Target
      do! Json.write "result" cr.Result
    }

type DomainDir =
  | FileName of string
  | FileContent of byte [] * string
  | ErrorMessage of string
  | ContentInfo of string * ContentInformation list
  | Rename of JsonRenameItem
  | Create of JsonCreateDir
  | Remove of JsonRemoveItem
  | CommieRest of CommieRest
  static member ToJson content =
    match content with
    | FileName fileName -> json { do! Json.write "filename" fileName }
    | FileContent (_, name) -> json { do! Json.write "name" name }
    | ErrorMessage errorMessage -> json { do! Json.write "error" errorMessage }
    | ContentInfo (url, infoList) ->
      json {
        do! Json.write "url" (urlize url)
        do! Json.write "listinfo" infoList
      }
    | Rename rename -> JsonRenameItem.ToJson rename
    | Remove remove -> JsonRemoveItem.ToJson remove
    | Create create -> JsonCreateDir.ToJson create
    | CommieRest cr -> CommieRest.ToJson cr
