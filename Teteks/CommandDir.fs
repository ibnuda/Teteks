﻿module CommandDir

open System.IO

open HttpMultipartParser

open Utilities.DirUtil
open Utilities.TextUtil
open DomainDir


let dirName (content : DomainDir) =
  match content with
  | DomainDir.ContentInfo (path, _) -> urlize path
  | DomainDir.FileContent (_, fileName) -> fileName
  | DomainDir.FileName message
  | DomainDir.ErrorMessage message -> message
  | DomainDir.Remove _ | DomainDir.Create _
  | DomainDir.Rename _ | DomainDir.CommieRest _ -> "Not Possible"

let runFromEx (f : unit -> DomainDir) =
  try
    f ()
  with
  | :? System.UnauthorizedAccessException -> DomainDir.ErrorMessage "Unauthorized access."
  | :? System.ArgumentNullException -> DomainDir.ErrorMessage "Null argument."
  | :? System.IO.DirectoryNotFoundException -> DomainDir.ErrorMessage "Directory not found."
  | :? System.IO.PathTooLongException -> DomainDir.ErrorMessage "Path too long."
  | :? System.IO.IOException -> DomainDir.ErrorMessage "IO Exception."
  | :? System.ArgumentOutOfRangeException -> DomainDir.ErrorMessage "Out of range."
  | :? System.ArgumentException -> DomainDir.ErrorMessage "Argument Exception."
  | :? System.NotSupportedException -> DomainDir.ErrorMessage "Haven't been written."
  | :? System.ObjectDisposedException -> DomainDir.ErrorMessage "Object disposed."
  | :? System.NullReferenceException -> DomainDir.ErrorMessage "Null reference."
  | :? System.InsufficientMemoryException -> DomainDir.ErrorMessage "Insufficient memory."
  | :? System.OutOfMemoryException -> DomainDir.ErrorMessage "Out of memory."

let readFromStream input (target : string list) = 
  let parser = new MultipartFormDataParser(input)
  try
    target
    |> List.map (fun key -> key, parser.GetParameterValue key)
    |> Map.ofList
  with
  | :? MultipartParseException -> Map.empty
  | :? System.NullReferenceException -> Map.empty

let readFileWithoutEx path () =
  let path = urlListToDiskPath path
  match File.Exists(path) with
  | true ->
    use stream = File.Open(path, FileMode.Open)
    use memstr = new MemoryStream()
    stream.CopyTo(memstr)
    stream.Close()
    let file = memstr.ToArray()
    memstr.Close()
    DomainDir.FileContent(file, "")
  | false -> DomainDir.ErrorMessage "There's no such thing."

let contentInfo path = 
  let attr = File.GetAttributes(path)
  if attr.HasFlag FileAttributes.Directory then
    let info = DirectoryInfo path
    Some (ContentInformation.Dir info)
  else
    let info = FileInfo path
    Some (ContentInformation.Fil info)

let contentListOfDirectoryWithoutEx (path : string list) () = 
  let p = Array.ofList (dir :: path)
  let path = Path.Combine(p)
  let dir = Directory.GetDirectories(path)
  let fil = Directory.GetFiles(path)
  let infoList =
    Array.append dir fil
    |> Array.map contentInfo
    |> Array.filter Option.isSome
    |> Array.map (fun i -> i.Value)
    |> List.ofArray
  DomainDir.ContentInfo (path, infoList)

// I put it here because this part is being used by other functions.
let contentListOfDirectory path = runFromEx (contentListOfDirectoryWithoutEx path)

let writeDataToDiskWithoutEx path stream () =
  let parser = new MultipartFormDataParser(stream)
  let file = parser.Files.Item(0)
  let filename = file.FileName |> sanitizeUrl
  let fileContent = file.Data
  let path = urlListToDiskPathAndAppend path [ filename ]
  use reader = new BinaryReader(fileContent)
  let fs = File.Create(path)
  reader.BaseStream.CopyTo(fs) |> ignore
  fs.Close()
  DomainDir.FileName path
  
let createDirectoryWithoutEx (parentpath : string list) (directoryname : string list) () =
  let newpat = List.append parentpath directoryname
  let newdir = urlListToDiskPathAndAppend parentpath directoryname
  match File.Exists newdir, Directory.Exists newdir with
  | true, _ | _, true ->
    CommieRest.Create ("createdir", newpat, "already exists.") |> DomainDir.CommieRest
  | false, false ->
    newdir |> Directory.CreateDirectory |> ignore
    let res =
      if Directory.Exists newdir then  
        CommieRest.Create ("createdir", newpat, "ok")
      else
        CommieRest.Create ("createdir", newpat, "bad")
    DomainDir.CommieRest res

let renameItemWithoutEx ppo ppn pio pin () =
  let olditem = urlListToDiskPathAndAppend ppo [ pio ]
  let newitem = urlListToDiskPathAndAppend ppn [ pin ]
  let oldpath = List.append ppo [ pio ]
  let newpath = List.append ppn [ pin ]
  let result =
    match File.Exists olditem, File.Exists newitem,
          Directory.Exists olditem, Directory.Exists newitem with
    | true, true, _, _
    | _, _, true, true
    | true, _, _, true
    | _, true, true, _ ->
      CommieRest.Create ("renameitem", newpath , "already exists.")
    | true, _, _, _ ->
      File.Move(olditem, newitem) |> ignore
      if File.Exists newitem then
        CommieRest.Create ("renameitem", newpath, "file moved.")
      else
        CommieRest.Create ("renameitem", newpath, "couldn't move file.")
    | _, _, true, _ ->
      Directory.Move(olditem, newitem) |> ignore
      if Directory.Exists newitem then  
        CommieRest.Create ("renameitem", newpath, "directory moved.")
      else
        CommieRest.Create ("renameitem", newpath, "couldn't move directory.")
    | _, _, _, _ ->
      CommieRest.Create ("renameitem", oldpath, "not found.")
  DomainDir.CommieRest result
  
let deleteItemWithoutEx (parentpath : string list) (target : string) () =
  let path = List.append parentpath [ target ]
  let pathtarget = urlListToDiskPathAndAppend parentpath [ target ]
  let res = 
    match File.Exists pathtarget, Directory.Exists pathtarget with
    | true, _ ->
      File.Delete pathtarget
      if File.Exists pathtarget then
        CommieRest.Create ("deleteitem", path, "couldn't delete file.")
      else
        CommieRest.Create ("deleteitem", path, "file deleted.")
    | _, true ->
      Directory.Delete (pathtarget, true)
      if Directory.Exists pathtarget then  
        CommieRest.Create ("deleteitem", path, "couldn't delete directory.")
      else
        CommieRest.Create ("deleteitem", path, "directory deleted.")
    | _, _ ->
      CommieRest.Create ("deleteitem", path, "you can't delete what doesn't exist.")
  DomainDir.CommieRest res

let readFile path = runFromEx (readFileWithoutEx path)
let writeFile path stream = runFromEx (writeDataToDiskWithoutEx path stream)
let createDirectory parentpath directoryname =
  runFromEx (createDirectoryWithoutEx parentpath directoryname)
let renameItem parentpathold parentpathnew pathitemold pathitemnew =
  runFromEx (renameItemWithoutEx parentpathold parentpathnew pathitemold pathitemnew)
let deleteItem parentpath target = runFromEx (deleteItemWithoutEx parentpath target)

let createDirectoryFromForm parentpath input =
  let parameters = readFromStream input [ "dirname" ]
  let dirname = parameters |> Map.find "dirname"
  createDirectory parentpath [ dirname ]

let renameItemFromForm parentpath input =
  let parameters = readFromStream input [ "parentnew"; "oldname"; "newname" ]
  // shit code.
  let newpath = parameters |> Map.find "parentnew" |> fun x -> x.Split('/') |> Array.toList
  let oldname = parameters |> Map.find "oldname"
  let newname = parameters |> Map.find "newname"
  renameItem parentpath newpath oldname newname

let deleteItemFromForm parentpath input =
  let deletetarget = readFromStream input [ "target" ] |> Map.find "target"
  deleteItem parentpath deletetarget

let apiCreateDirectory (jsoncreatedir : JsonCreateDir option) =
  match jsoncreatedir with
  | Some jsoncreatedir ->
    let parentpath = jsoncreatedir.PathParent
    let newname = jsoncreatedir.PathDir
    createDirectory parentpath newname
  | None ->
    CommieRest.Create ("create", [], "invalid json.") |> DomainDir.CommieRest

let apiRenameItem (jsonrename : JsonRenameItem option) =
  match jsonrename with
  | Some jsonrename ->
    let parentpathold = jsonrename.PathParentOld
    let parentpathnew = jsonrename.PathParentNew
    let oldname = jsonrename.PathItemOld
    let newname = jsonrename.PathItemNew
    renameItem parentpathold parentpathnew oldname newname
  | None ->
    CommieRest.Create ("rename", [], "invalid json.") |> DomainDir.CommieRest

let apiDeleteItem (jsondelete : JsonRemoveItem option) =
  match jsondelete with
  | Some jsondelete ->
    let parentpath = jsondelete.PathParent
    let target = jsondelete.PathItem
    deleteItem parentpath target
  | None ->
    CommieRest.Create ("delete", [], "invalid json.") |> DomainDir.CommieRest

type InformasiDirektori =
  { Informasi : DirectoryInfo
    KontenDirektori : InformasiDirektori list
    KontenBerkas : InformasiBerkas list }
and InformasiBerkas =
  { Informasi : FileInfo }

let getinfo path = ()

let rekursi path =
  let rec something path =
    seq {
      yield! Directory.GetFiles path
      for dir in Directory.GetDirectories path do
        yield! something dir
    }
  something path
