﻿[<RequireQualifiedAccess>]
module Database

open FSharp.Data.Sql

[<Literal>]
let ConnString = "DataSource=" +
                  __SOURCE_DIRECTORY__ +
                  @"/teteks.db;" +
                  "Version=3;foreign keys=true"

[<Literal>]
let ResPath = __SOURCE_DIRECTORY__ +
              @"/../packages/System.Data.SQLite.Core.1.1.103/lib/net46"

type Sql = SqlDataProvider<
            Common.DatabaseProviderTypes.SQLITE,
            ConnectionString=ConnString,
            ResolutionPath=ResPath,
            CaseSensitivityChange=Common.CaseSensitivityChange.ORIGINAL>

type DbContext = Sql.dataContext

type DbStory = DbContext.``main.ceritaEntity``

let gcont = Sql.GetDataContext()
let firstOrNone (s : seq<'a>) : 'a option = s |> Seq.tryFind (fun _ -> true)
let isExist x = Seq.exists ((=) x)

let getStory url (cont : DbContext) = 
  query { 
    for story in cont.Main.Cerita do
      where (story.Url = url)
      select story
  }
  |> firstOrNone

let isInDb url (cont : DbContext) = 
  query { 
    for story in cont.Main.Cerita do
      exists (story.Url = url)
  }

let canEdit url password (cont : DbContext) = 
  query { 
    for cerita in cont.Main.Cerita do
      exists (cerita.Url = url && cerita.Sandi = password)
  }

let saveStory (url, password, content, tc, tt, ta, td) (cont : DbContext) = 
  match isInDb url cont with
  | true -> None
  | false -> 
    let story = cont.Main.Cerita.Create()
    story.Url <- url
    story.Sandi <- password
    story.Isi <- content
    story.TwCard <- tc
    story.TwTitle <- tt
    story.TwAuthor <- ta
    story.TwDesc <- td
    cont.SubmitUpdatesAsync()
    |> Async.StartAsTask
    |> ignore
    Some story

let editStory (oldUrl, url, password, newPass, content, tc, tt, ta, td) (cont :DbContext) = 
  match canEdit oldUrl password cont with
  | true -> 
    // unneeded, actually.
    match getStory oldUrl cont with
    | Some story -> 
      story.SetColumn("Url", url)
      story.SetColumn("Sandi", newPass)
      story.SetColumn("Isi", content)
      story.SetColumn("TwCard", tc)
      story.SetColumn("TwTitle", tt)
      story.SetColumn("TwAuthor", ta)
      story.SetColumn("TwDesc", td)
      cont.SubmitUpdatesAsync()
      |> Async.StartAsTask
      |> ignore
      Some story
    | None -> None
  | false -> None
