﻿[<AutoOpen>]
module Common

open System

module Async = 
  let map f x =
    async {
      let! v = x
      return f v
    }

module Option = 
  let fromNullable = 
    function 
    | null -> None
    | x -> Some x

module Milih = 

  let Nice = Choice1Of2
  let Naah = Choice2Of2

  let (|Nice|Naah|) m = 
    match m with
    | Choice1Of2 x -> Nice x
    | Choice2Of2 x -> Naah x

  let bind f m = 
    match m with
    | Nice x -> f x
    | Naah _ -> None

  let compose fb fa x =
    match fa x with
    | Nice a -> fb a
    | Naah e -> Naah e

  let toOption = 
    function 
    | Nice x -> Some x
    | Naah _ -> None

  let (>>=) m f = bind f m
  let (>=>) fa fb = compose fb fa
  let (?^) x = toOption x

module Tuple = 
  let map f (x, y) = f x, f y

module Utils = 

  type MaybeBuilder() = 
    member __.Bind(m, f) = Option.bind f m
    member __.Return(x) = Some x
    member __.ReturnFrom(x) = x

  let maybe = MaybeBuilder()

