﻿module HandlerStory

open Freya.Core
open Freya.Core.Operators
open Freya.Optics.Http
open Freya.Core

open Utilities.RequestUtil
open Utilities.TextUtil

open DomainStory
open CommandStory

let newStoryPost : Freya<NewStory option> =
  Freya.Optic.get Request.body_
  >>= (newStoryFromForm >> Freya.init)

let editStoryPost : Freya<EditStory option> =
  Freya.Optic.get Request.body_
  >>= (editStoryFromForm >> Freya.init)

let storyCreateHandler =
  Freya.memo (newStoryPost >>= fun ns ->
              Freya.fromAsync(ns, Pemroses.storyCreate))

let storyUpdateHandler =
  Freya.memo (requestUrl >>= fun u ->
              editStoryPost >>= fun us ->
              Freya.fromAsync ((u, us), Pemroses.storyUpdate))

let apiStoryReadHandler =
  Freya.memo (requestUrl >>= fun url ->
              Freya.fromAsync (url, Pemroses.storyRead))

let apiStoryUpdateHandler = 
  Freya.memo (requestUrl >>= fun u ->
              payload() >>= fun p ->
              Freya.fromAsync ((u, p), Pemroses.storyUpdate))

let apiStoryCreateHandler =
  Freya.memo (payload() >>= fun p ->
              Freya.fromAsync (p, Pemroses.storyCreate))

let lostHandler =
  Freya.memo (requestUrlList >>= fun u ->
              Freya.fromAsync (lostToString u, Pemroses.lostLostLost))
