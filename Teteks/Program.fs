﻿module Program

module Server = 
  open Freya.Core
  
  type StoryBackend() = 
    member __.Configuration() = OwinAppFunc.ofFreya (Machinery.ceritaRuter)

open System
open Microsoft.Owin.Hosting

[<EntryPoint>]
let main _ = 
  WebApp.Start<Server.StoryBackend>("http://localhost:6777") |> ignore
  Console.ReadLine() |> ignore
  0
