﻿module DomainStory

open System.IO
open Chiron
open Chiron.Operators

type Story = 
  { Url : string
    Content : string
    TCard : string
    TTitle : string
    TDesc : string
    TAuthor : string }

  static member FromJson(_ : NewStory) = 
    json { 
      let! u = Json.read "url"
      let! i = Json.read "content"
      let! tc = Json.read "tcard"
      let! tt = Json.read "ttitle"
      let! td = Json.read "tdesc"
      let! ta = Json.read "tauthor"
      return (Story.Create (u, i, tc, tt, td, ta))
    }

  static member ToJson(story: Story) = 
      json { 
        do! Json.write "url" story.Url
        do! Json.write "content" story.Content
        do! Json.write "tcard" story.TCard
        do! Json.write "ttitle" story.TTitle
        do! Json.write "tdesc" story.TDesc
        do! Json.write "tauthor" story.TAuthor
      }
    
  static member Create(url, content, tc, tt, td, ta) = 
    { Url = url
      Content = content
      TCard = tc
      TTitle = tt
      TDesc = td
      TAuthor = ta }

and NewStory = 
  { Url : string option
    Password : string option
    Content : string option
    TCard : string option
    TTitle : string option
    TAuthor : string option
    TDesc : string option }

  static member FromJson(_ : NewStory) = 
    json { 
      let! u = Json.tryRead "url"
      let! s = Json.tryRead "password"
      let! i = Json.tryRead "content"
      let! tc = Json.tryRead "tcard"
      let! tt = Json.tryRead "ttitle"
      let! td = Json.tryRead "tdesc"
      let! ta = Json.tryRead "tauthor"
      return (NewStory.Create(u, s, i, tc, tt, td, ta))
    }

  static member ToJson(newStory : NewStory) = 
      json { 
        do! Json.write "url" newStory.Url
        do! Json.write "content" newStory.Content
        do! Json.write "tcard" newStory.TCard
        do! Json.write "ttitle" newStory.TTitle
        do! Json.write "tdesc" newStory.TDesc
        do! Json.write "tauthor" newStory.TAuthor
      }

  static member Create(u, p, c, tc, tt, td, ta) = 
    { Url = u
      Password = p
      Content = c
      TCard = tc
      TTitle = tt
      TDesc = td
      TAuthor = ta }

  static member Empty() = 
    { Url = None
      Password = None
      Content = Some "weird shit."
      TCard = None
      TTitle = None
      TDesc = None
      TAuthor = None }

and EditStory = 
  { Url : string option
    Password : string
    NewPassword : string option
    Content : string option
    TCard : string option
    TTitle : string option
    TAuthor : string option
    TDesc : string option }
  
  static member FromJson(_ : EditStory) = 
    json { 
      let! u = Json.tryRead "url"
      let! p = Json.read "password"
      let! np = Json.tryRead "newpassword"
      let! c = Json.tryRead "content"
      let! tc = Json.tryRead "tcard"
      let! tt = Json.tryRead "ttitle"
      let! td = Json.tryRead "tdesc"
      let! ta = Json.tryRead "tauthor"
      return (EditStory.Create (u, p, np, c, tc, tt, ta, td))
    }

  static member ToJson(editStory : EditStory) = 
      json { 
        do! Json.write "url" editStory.Url
        do! Json.write "password" editStory.Password
        do! Json.write "newpassword" editStory.NewPassword
        do! Json.write "content" editStory.Content
        do! Json.write "tcard" editStory.TCard
        do! Json.write "ttitle" editStory.TTitle
        do! Json.write "tdesc" editStory.TDesc
        do! Json.write "tauthor" editStory.TAuthor
      }
    
  static member Create(u, p, np, c, tc, tt, ta, td) = 
    { Url = u
      Password = p
      NewPassword = np
      Content = c
      TCard = tc
      TTitle = tt
      TDesc = td
      TAuthor = ta }

  static member Empty() = 
    { Url = None
      Password = ""
      NewPassword = None
      Content = None
      TCard = None
      TTitle = None
      TDesc = None
      TAuthor = None }

and Message = 
  { Action : string
    Url : string
    Content : string }

  static member ToJson(message : Message) = 
    json {
      do! Json.write "action" message.Action
      do! Json.write "url" message.Url
      do! Json.write "content" message.Content
    }

  static member Create(action, url, content) =
    { Action = action
      Url = url
      Content = content }

type DomainStory = 
  | Story of Story
  | NewStory of NewStory
  | EditStory of EditStory
  | Message of Message
  static member ToJson data = 
    match data with
    | Story story -> Story.ToJson story
    | NewStory newStory ->  NewStory.ToJson newStory
    | EditStory editStory -> EditStory.ToJson editStory
    | Message message -> Message.ToJson message

