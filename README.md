# Teteks

Actually, this project was intended as a clone of [txti.es](http://txti.es) for learning purpose. 

But somehow, I extend it to have capabilities like [nya.is](http://nya.is).

## Library Used
- [Freya stack](https://freya.io)
- [F# SQLProvider](https://fsprojects.github.io/SQLProvider/)
- [FSharp.Formatting](https://tpetricek.github.io/FSharp.Formatting/)

## Features Completed
### Txti.es features
- Create content.
- Edit content.
- Password thingy.
- REST api thingy.

### Nya.is features
- File upload.
- File download.

### Other features
- Directory and shit.

## Next Features?
- See TODOS.md

## License
MIT License. Or whatever my company want it to be.
